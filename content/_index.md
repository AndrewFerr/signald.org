---
title: signald
---
*an API for Signal. Not official and [not nearly as secure as first party Signal clients](https://gitlab.com/signald/signald/-/issues/101)*

[![source](/badges/stars.svg)](https://gitlab.com/signald/signald)
[![docker](/badges/docker.svg)](https://hub.docker.com/r/signald/signald)
[![GPL](/badges/license.svg)](https://gitlab.com/signald/signald/-/blob/main/LICENSE)

[source](https://gitlab.com/signald/signald) | [issues](https://gitlab.com/signald/signald/-/issues)

Signal does not offer any sort of official API. Their server software is [open source](https://github.com/SignalApp/Signal-Server) and anyone can
figure out the REST endpoints, but building the payloads turns out to be a little trickier. Signal, unlike traditional messaging applications,
puts a number requirements around encryption and account data storage on the client software. signald aims to handle all of the Signal-specific
requirements, exposing an API that sends and receives unencrypted messages with other software on the same machine.

signald does not come with a user friendly frontend. Users should select an existing [client](/articles/clients/) to use signald with.

Developers wanting to learn how to write code to interact with signald, check out [getting started](/articles/getting-started/) page.