# Installation from source

1. Install Java
1. Clone the signald repository:

    ```shell
    git clone https://gitlab.com/signald/signald.git
    ```

1. Build signald:

   ```shell
   cd signald
   make installDist
   ```

1. Configure the system directories:

   ```
   make setup
   ``` 

1. Start signald:

   ```shell
   build/install/signald/bin/signald
   ```

   It will continue running until killed (or <kbd>Ctrl-C</kbd>).

To make it start automatically at startup, systemd is typically used. A sample systemd service
file may be found at `src/main/resources/io/finn/signald/signald.service`

## Additional tooling

Consider installing [signaldctl](/signaldctl/).
