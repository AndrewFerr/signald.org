---
title: Request IDs
aliases:
    - /articles/request-ids/
---

All requests to signald may contain an `"id"` field with a string value. Any responses to this request
with include an `"id"` field with the same value. These IDs may be referenced in signald logs, so
avoid putting any kind of identifying information in them. Many clients use UUIDs for request IDs,
but there is no requirement to do so.